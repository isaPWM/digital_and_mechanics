package pt.isec.GPS.DigitalMechanics.Model.dataClasses;

import java.io.Serial;
import java.io.Serializable;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class ListaLembretes implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;
    private PriorityQueue<Lembrete> lista;

    public ListaLembretes() {
        lista = new PriorityQueue<>(
            new Comparator<Lembrete>() {
                @Override
                public int compare(Lembrete l1, Lembrete l2) {
                    return l1.compareTo(l2);
                }
            });
    }
    public ListaLembretes(ListaLembretes listaLembretesAtual) {
        lista = new PriorityQueue<>(
                new Comparator<Lembrete>() {
                    @Override
                    public int compare(Lembrete l1, Lembrete l2) {
                        return l1.compareTo(l2);
                    }
                });
        lista.addAll(listaLembretesAtual.lista);
    }

    public int size() {
        return lista.size();
    }

    public int getNumNotificacoes() {
        AtomicInteger num = new AtomicInteger();
        lista.forEach(lembrete -> num.addAndGet((lembrete.isEstadoAberto() ? 0 : 1)));
        return num.get();
    }

    public Lembrete[] toArray() {
        return lista.toArray(new Lembrete[0]);
    }

    public void addLembrete(Lembrete novo) {
        lista.add(novo);
    }

    public void mudaParaVisto(int id) {
        for(Lembrete lembrete : lista)
            if(lembrete.getId() == id) {
                lembrete.setEstadoAberto(true);
                break;
            }
    }
    public void mudaParaNaoVisto(int id) {
        for(Lembrete lembrete : lista)
            if(lembrete.getId() == id) {
                lembrete.setEstadoAberto(false);
                break;
            }
    }
    public void remove(int id) {
        lista.removeIf(lembrete -> lembrete.getId() == id);
    }

    public void removeVistos() {
        lista.removeIf(Lembrete::isEstadoAberto);
    }

    public void clear() {
        lista.clear();
    }

    public void reset(ListaLembretes listaLembretesAtual) {
        lista.clear();
        lista.addAll(listaLembretesAtual.lista);
    }
}
