package pt.isec.GPS.DigitalMechanics.Model.dataClasses;

import pt.isec.GPS.DigitalMechanics.Model.TipoBloco;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;

public class AppDigitalMechanics implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;
    private ArrayList<InfoRegistoVeiculo> listaRegistosVeiculos;
    private int indiceRegistoAtual;
    private ListaLembretes listaLembretesAtual;
    private BlocoInformacao blocoInformacao;
    private int ID_LEMBRETE;

    // UTILIZAÇÃO INFO
    private int numImportacoes, numExportacoes, numVeiculos;
    ////////////////////////////////////////////////////////

    public AppDigitalMechanics() {
        listaRegistosVeiculos = new ArrayList<>();
        indiceRegistoAtual = 0;
        numImportacoes = 0;
        numExportacoes = 0;
        numVeiculos = 0;
        blocoInformacao = null;
        ID_LEMBRETE = 0;
    }

    public void verificaExistenciaDados() {
        if(listaRegistosVeiculos.isEmpty())
            return;
        listaRegistosVeiculos.removeIf(infoRegistoVeiculo -> !infoRegistoVeiculo.verificaExistenciaDados());
    }

    //////////////////////////////////////////////////////////
    /*-------------------------GETTERS---------------*/
    public int getNumImportacoes() {
        return numImportacoes;
    }
    public int getNumExportacoes() {
        return numExportacoes;
    }
    public int getNumVeiculos() {
        return numVeiculos;
    }
    public String getNomeBlocoInfoAtual() {
        return blocoInformacao.getNomeBlocoInfo();
    }
    public String[] getNomesRegistosVeiculos() {
        String [] lista = new String[listaRegistosVeiculos.size()];
        listaRegistosVeiculos.forEach(infoRegistoVeiculo -> lista[listaRegistosVeiculos.indexOf(infoRegistoVeiculo)] = infoRegistoVeiculo.getNomeRegisto());
        return lista;
    }
    public String[] getNomesBlocosInfo() {
        return listaRegistosVeiculos.get(indiceRegistoAtual).getNomeBlocosInfo();
    }
    public ArrayList<Record> getListaRegistosDeBlocoAtual() {
        return blocoInformacao.getListaRegistos();
    }
    public TipoBloco getTipoBlocoInfo() {
        return blocoInformacao.getTipoBlocoInfo();
    }
    public Lembrete[] getListaLembretes() {
        return listaLembretesAtual.toArray();
    }
    public int getNumNotificacoes() {
        return listaLembretesAtual.getNumNotificacoes();
    }
    public int getListaLembretesSize() {
        return listaLembretesAtual.size();
    }
    private void adicionaLembrete(Lembrete novo) {
        listaLembretesAtual.addLembrete(novo);
    }
    public void eliminaLembrete(int indice) {
        listaLembretesAtual.remove(indice);
    }
    public void mudaParaVisto(int indice) {
        listaLembretesAtual.mudaParaVisto(indice);
    }
    public void mudaParaNaoVisto(int indice) {
        listaLembretesAtual.mudaParaNaoVisto(indice);
    }
    public void eliminaLembretesVistos() {
        listaLembretesAtual.removeVistos();
    }
    public void limparLembretes() {
        listaLembretesAtual.clear();
    }

    //////////////////////////////////////////////////////////
    /*-------------------------VERIFICAÇÕES---------------*/
    private boolean matriculaValida(String matricula, int ano)
    {
        if(   (ano < 1992 && matricula.matches("^[A-Z]{2}+\\-+[0-9]{2}+\\-+[0-9]{2}+$"))
           || (ano >= 1992 && ano < 2005 && matricula.matches("^[0-9]{2}+\\-+[0-9]{2}+\\-+[A-Z]{2}+$"))
           || (ano >= 2005 && ano < 2020 && matricula.matches("^[0-9]{2}+\\-+[A-Z]{2}+\\-+[0-9]{2}+$"))
           || (ano >= 2020 && matricula.matches("^[A-Z]{2}+\\-+[0-9]{2}+\\-+[A-Z]{2}+$"))) {
            return true;
        }
        return false;
    }

    ////////////////////////////////////////////////////////////
    /*-------------------------ADICIONA REGISTO---------------*/

    /*------------------------------VEÍCULO-------------------*/
    public boolean adicionaRegistoVeiculo(String marca, String modelo, String matricula, String combustivel, int ano, int mes, String quilometros) {
        if(marca == null || marca.isBlank() || modelo == null || modelo.isBlank() || combustivel == null || combustivel.isBlank() || quilometros == null || quilometros.isBlank() || matricula == null || matricula.isBlank() || !matriculaValida(matricula, ano))
            return false;

        try {
            long aux = Long.parseLong(quilometros);
            if(aux >= 0) {
                for (InfoRegistoVeiculo info : listaRegistosVeiculos)
                    if (info.getNomeRegisto().equals(matricula))
                        return false;
                try {
                    InfoRegistoVeiculo infoRegistoVeiculo = new InfoRegistoVeiculo(marca, modelo, matricula, combustivel, ano, mes, Long.parseLong(quilometros));
                    listaRegistosVeiculos.add(infoRegistoVeiculo);
                    numVeiculos++;
                    return true;
                } catch (Exception ignored) {}
            }
        } catch (Exception ignored) {}
        return false;
    }

    /*---------------------------ABASTECIMENTO----------------*/
    public InfoRetorno adicionaAbastecimento(String preco, String litros, String quilometros, LocalDate data) {
        if(blocoInformacao == null)
            return null;
        InfoRetorno res = blocoInformacao.adicionaAbastecimento(preco, litros, quilometros, data, listaRegistosVeiculos.get(indiceRegistoAtual).getQuilometros());
        if(res.resultado()){
            resaveBlocoInformacao();
        }
        return res;
    }

    /*--------------------------------PEÇA--------------------*/
    public InfoRetorno adicionaPeca(String nome, String marca, String modelo, String preco, String descricao, String loja, LocalDate data, String foto) {
        if(blocoInformacao == null)
            return null;
        InfoRetorno res = blocoInformacao.adicionaPeca(nome, marca, modelo, preco, descricao, loja, data, foto);
        if(res.resultado()){
            resaveBlocoInformacao();
        }
        return res;
    }

    /*--------------------------MUDANÇA DE ÓLEO---------------*/
    public InfoRetorno adicionaMudancaOleo(LocalDate data, String text, String foto) {
        if(blocoInformacao == null)
            return null;
        InfoRetorno res =  blocoInformacao.adicionaMudancaOleo(data,text,foto);
        if(res.resultado()){
            resaveBlocoInformacao();
        }
        return res;
    }

    /*-----------------------------MANUTENÇÃO-----------------*/
    public InfoRetorno adicionaManutencao(String tipo, LocalDate data, String preco, String observacao) {
        if(blocoInformacao == null)
            return null;
        InfoRetorno res =  blocoInformacao.adicionaManutencao(tipo, data, preco, observacao);
        if(res.resultado()){
            resaveBlocoInformacao();
        }
        return res;
    }

    /*----------------------------PERSONALIZADO---------------*/
    public InfoRetorno adicionaRegistoPersonalizado(String nome, LocalDate data, String preco, String descricao) {
        if(blocoInformacao == null)
            return null;
        InfoRetorno res =  blocoInformacao.adicionaRegistoPersonalizado(nome, data, preco, descricao);
        if(res.resultado()){
            resaveBlocoInformacao();
        }
        return res;
    }


    ////////////////////////////////////////////////////////////
    /*-------------------------ADICIONA BLOCOS---------------*/
    public boolean adicionaBlocoInformacao(String nomeBloco) {
        if(listaRegistosVeiculos == null)
            return false;
        return listaRegistosVeiculos.get(indiceRegistoAtual).adicionaBlocoInformacao(nomeBloco, TipoBloco.OUTRO);
    }

    public boolean resaveBlocoInformacao() {
        return listaRegistosVeiculos.get(indiceRegistoAtual).saveBlocoInformacao(blocoInformacao);
    }

    ////////////////////////////////////////////////////////////
    /*----------------------------SELEÇÕES-------------------*/
    public void selecionaRegistoVeiculo(String nomeRegisto) {
        if (listaRegistosVeiculos.size() == 0)
            return;
        for(InfoRegistoVeiculo infoRegistoVeiculo : listaRegistosVeiculos)
            if(infoRegistoVeiculo.getNomeRegisto().equals(nomeRegisto)){
                indiceRegistoAtual = listaRegistosVeiculos.indexOf(infoRegistoVeiculo);
                listaLembretesAtual = listaRegistosVeiculos.get(indiceRegistoAtual).getListaLembretes();
                break;
            }
    }
    public void desSelecionaRegistoVeiculo() {
        listaRegistosVeiculos.get(indiceRegistoAtual).resetListaLembretes(listaLembretesAtual);
    }
    public boolean selecionaBlocoInformacao(String nomeBloco) {
        if(listaRegistosVeiculos.size() == 0)
            return false;
        blocoInformacao = listaRegistosVeiculos.get(indiceRegistoAtual).getBlocoInformacao(nomeBloco);
        return blocoInformacao != null;
    }
}
