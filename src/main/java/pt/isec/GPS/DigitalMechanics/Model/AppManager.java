package pt.isec.GPS.DigitalMechanics.Model;

import pt.isec.GPS.DigitalMechanics.Model.dataClasses.AppDigitalMechanics;
import pt.isec.GPS.DigitalMechanics.Model.dataClasses.InfoRetorno;
import pt.isec.GPS.DigitalMechanics.Model.dataClasses.Lembrete;

import java.io.*;
import java.time.LocalDate;
import java.util.ArrayList;

public class AppManager {
    private static final String FILE = "src/main/resources/appFiles/AppDigitalMechanics.txt";
    private AppDigitalMechanics appDigitalMechanics;

    public AppManager() {
        load();
    }

    //--------------------------- GETTERS --------------------------

    public String[] getNomesRegistosVeiculos() {
        return appDigitalMechanics.getNomesRegistosVeiculos();
    }
    public String[] getNomesBlocosInfo() {
        return appDigitalMechanics.getNomesBlocosInfo();
    }
    public int getNumVeiculos() {
        return appDigitalMechanics.getNumVeiculos();
    }
    public int getNumImportacoes() {
        return appDigitalMechanics.getNumImportacoes();
    }
    public int getNumExportacoes() {
        return appDigitalMechanics.getNumExportacoes();
    }
    public String getNomeBlocoInfoAtual() {
        return appDigitalMechanics.getNomeBlocoInfoAtual();
    }
    public ArrayList<Record> getListaRegistosDeBlocoAtual() {
        return appDigitalMechanics.getListaRegistosDeBlocoAtual();
    }
    public Lembrete[] getListaLembretes() {
        return appDigitalMechanics.getListaLembretes();
    }
    public int getNumNotificacoes() {
        return appDigitalMechanics.getNumNotificacoes();
    }
    public int getListaLembretesSize() {
        return appDigitalMechanics.getListaLembretesSize();
    }
    public void eliminaLembrete(int indice) {
        appDigitalMechanics.eliminaLembrete(indice);
    }
    public void mudaParaVisto(int indice) {
        appDigitalMechanics.mudaParaVisto(indice);
    }
    public void mudaParaNaoVisto(int indice) {
        appDigitalMechanics.mudaParaNaoVisto(indice);
    }
    public void eliminaLembretesVistos() {
        appDigitalMechanics.eliminaLembretesVistos();
    }
    public void limparLembretes() {
        appDigitalMechanics.limparLembretes();
    }
    //--------------------------- SETTERS --------------------------

    //------------------ ADIÇÃO / REMOÇÃO DE INFO ------------------
    public boolean adicionaRegistoVeiculo(String marca, String modelo, String matricula, String combustivel, int ano, int mes, String quilometros){
        return appDigitalMechanics.adicionaRegistoVeiculo(marca, modelo, matricula, combustivel, ano, mes, quilometros);
    }
    public boolean adicionaBlocoInformacao(String nomeBlocoInfo){
        return appDigitalMechanics.adicionaBlocoInformacao(nomeBlocoInfo);
    }

    public InfoRetorno adicionaAbastecimento(String preco, String litros, String quilometros, LocalDate data){
        return appDigitalMechanics.adicionaAbastecimento(preco,litros,quilometros,data);
    }

    public InfoRetorno adicionaPeca(String nome, String marca, String modelo, String preco, String descricao, String loja, LocalDate data, String foto){
        return appDigitalMechanics.adicionaPeca(nome, marca, modelo, preco, descricao, loja, data, foto);
    }
    public InfoRetorno adicionaMudancaOleo(LocalDate data, String text, String foto) {
        return appDigitalMechanics.adicionaMudancaOleo(data,text,foto);
    }

    public InfoRetorno adicionaManutencao(String tipo, LocalDate data, String preco, String observacao){
        return appDigitalMechanics.adicionaManutencao(tipo, data, preco, observacao);
    }

    public InfoRetorno adicionaRegistoPersonalizado(String nome, LocalDate data, String preco, String descricao) {
        return appDigitalMechanics.adicionaRegistoPersonalizado(nome, data, preco, descricao);
    }

    //------------------------- SELEÇÕES ---------------------------
    public void selecionaRegistoVeiculo(String nomeRegisto){
        appDigitalMechanics.selecionaRegistoVeiculo(nomeRegisto);
    }
    public void desSelecionaRegistoVeiculo() {
        appDigitalMechanics.desSelecionaRegistoVeiculo();
    }
    public boolean selecionaBlocoInformacao(String nomeBlocoInfo){
        return appDigitalMechanics.selecionaBlocoInformacao(nomeBlocoInfo);
    }

    //------------------ MANIPULAÇÃO DE FICHEIROS ------------------
    public void save() {
        try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(FILE))){
            oos.writeObject(appDigitalMechanics);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void load(){
        File file = new File(FILE);
        if(file.isFile()) {
            try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))) {
                appDigitalMechanics = (AppDigitalMechanics) ois.readObject();

                //Verifica existencia dos dados
                appDigitalMechanics.verificaExistenciaDados();
            } catch (IOException e) {
                e.printStackTrace();
                appDigitalMechanics = new AppDigitalMechanics();
            }
            catch (ClassNotFoundException e) {
                appDigitalMechanics = new AppDigitalMechanics();
            }
        }
        else {
            appDigitalMechanics = new AppDigitalMechanics();
        }
    }

    public TipoBloco getTipoBlocoInfo() {
        return appDigitalMechanics.getTipoBlocoInfo();
    }
}