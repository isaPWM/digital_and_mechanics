package pt.isec.GPS.DigitalMechanics.Model.dataRecords;

import java.io.Serial;
import java.io.Serializable;

public record MudancaOleo(String data , String marca, String foto) implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;
    @Override
    public String toString() {
        return "Marca: " + marca + "\n" +
                "Data: " + data + "\n" +
                "Foto: " + foto;
    }
}
