package pt.isec.GPS.DigitalMechanics.Model.dataClasses;

import pt.isec.GPS.DigitalMechanics.Model.TipoBloco;
import pt.isec.GPS.DigitalMechanics.Model.dataRecords.Auto;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class InfoRegistoVeiculo implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;
    private String FILE = "src/main/resources/appFiles/";
    private String imagemPerfilRegisto;
    private Auto veiculo;
    private long quilometros;
    private Map<String, String> mapAcessoBlocosInfo;
    private ListaLembretes listaLembretes;

    public InfoRegistoVeiculo(String marca, String modelo, String matricula, String combustivel, int ano, int mes, Long quilometros) {
        try {
            FILE += (matricula + "/");
            if (!new File(FILE).mkdirs())
                throw new Exception("Erro ao criar diretoria");
            veiculo = new Auto(marca, modelo, matricula, combustivel, ano, mes);
            this.quilometros = quilometros;
            imagemPerfilRegisto = null;

            mapAcessoBlocosInfo = new HashMap<>();
            adicionaBlocoInformacao("Manutenções", TipoBloco.MANUTENCOES);
            adicionaBlocoInformacao("Mudança Óleo", TipoBloco.MUDANCAOLEO);
            adicionaBlocoInformacao("Peças", TipoBloco.PECAS);
            adicionaBlocoInformacao("Abastecimentos", TipoBloco.ABASTECIMENTOS);
            listaLembretes = new ListaLembretes();
        } catch (Exception e) {
            System.out.println("Erro");
        }
    }
    public boolean verificaExistenciaDados(){
        if (!(new File(FILE)).exists()) {
            return false;
        }

        if(mapAcessoBlocosInfo.isEmpty())
            return true;
        for (String ficheiroBlocoInformacao : mapAcessoBlocosInfo.values())
            if(!(new File(ficheiroBlocoInformacao)).isFile())
                mapAcessoBlocosInfo.remove(ficheiroBlocoInformacao);
        return true;
    }
    //--------------------------- GETTERS --------------------------

    public long getQuilometros() {
        return quilometros;
    }

    public String[] getNomeBlocosInfo(){
        String [] lista = new String[mapAcessoBlocosInfo.size()];
        mapAcessoBlocosInfo.keySet().toArray(lista);
        return lista;
    }

    public String getNomeRegisto(){
        return veiculo.matricula();
    }

    public BlocoInformacao getBlocoInformacao(String nomeBlocoInfo){
        File ficheiro = new File(mapAcessoBlocosInfo.get(nomeBlocoInfo));
        if(ficheiro.isFile()) {
            try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(ficheiro))) {
                return (BlocoInformacao) ois.readObject();
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
            catch (ClassNotFoundException e) {
                return null;
            }
        }
        return null;
    }

    public ListaLembretes getListaLembretes() {
        return new ListaLembretes(listaLembretes);
    }
    public void resetListaLembretes(ListaLembretes listaLembretesAtual) {
        listaLembretes.reset(listaLembretesAtual);
    }
    //------------------ ADIÇÃO / REMOÇÃO DE INFO ------------------
    public boolean adicionaBlocoInformacao(String nomeBlocoInfo, TipoBloco tipo){
        try {
            mapAcessoBlocosInfo.put(nomeBlocoInfo, FILE + nomeBlocoInfo + ".txt");
            return saveBlocoInformacao(new BlocoInformacao(tipo, nomeBlocoInfo));
        } catch (Exception e) {
            return false;
        }
    }
    public boolean saveBlocoInformacao(BlocoInformacao bloco) {
        try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream( FILE + bloco.getNomeBlocoInfo() + ".txt"))){
            oos.writeObject(bloco);
            return true;
        }catch (Exception ignored){
        }
        return false;
    }

    /*public boolean removeBlocoInformacao(String nomeBlocoInfo, Peca p){
        try {
            mapAcessoBlocosInfo.remove(nomeBlocoInfo, FILE + nomeBlocoInfo + ".txt");
            saveBlocoInformacao(new BlocoPeca(p.getMarca(), p.getModelo(), p.getPreco(), p.getAmp(), p.getTamanho(), p.getBar(), p.getDescricao()), FILE + nomeBlocoInfo + ".txt");
            return true;
        } catch (Exception e) {
            return false;
        }
    }*/
    //------------------ SALVAR MUDANÇAS BLOCOS ------------------
    /*public void verificacaoInformacao() { //NAO USAR, causa o desaparecimento de blocos
        Iterator<Map.Entry<String, String>> iterator = mapAcessoBlocosInfo.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, String> entry = iterator.next();
            File aux = new File(entry.getValue());
            if (!aux.exists())
                iterator.remove();
        }
    }*/
}
