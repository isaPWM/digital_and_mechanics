package pt.isec.GPS.DigitalMechanics.UI.Buttons;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import pt.isec.GPS.DigitalMechanics.Model.dataRecords.Abastecimento;
import pt.isec.GPS.DigitalMechanics.Model.dataRecords.Manutencao;
import pt.isec.GPS.DigitalMechanics.Model.dataRecords.MudancaOleo;
import pt.isec.GPS.DigitalMechanics.Model.dataRecords.RegistoGeral;

public class BotaoRegisto extends Button {
    private Record registo;
    private Label textoInfoBox;
    private VBox infoBox;
    private FlowPane listaRegistosInfoScrollPane;
    public BotaoRegisto(Record registo, Label textoInfoBox, VBox infoBox, String tipoRegisto, FlowPane listaRegistosInfoScrollPane){
        this.textoInfoBox = textoInfoBox;
        this.infoBox = infoBox;
        this.listaRegistosInfoScrollPane = listaRegistosInfoScrollPane;
        this.registo = registo;
        createViews(tipoRegisto);
        registerHandlers();
        update();
    }

    private void createViews(String tipoRegisto) {
        if (registo instanceof Abastecimento a) {
            setText("Em " + a.data());
        }
        else if (registo instanceof Manutencao m) {
            setText(m.tipo() + " em " + m.data());
        }
        else if (registo instanceof MudancaOleo mo) {
            setText(tipoRegisto + " em " + mo.data());
        }
        else {
            setText(tipoRegisto + " em " + ((RegistoGeral)registo).data());
        }
        setPrefSize(250, 30);
        setStyle("-fx-font-family: Arial Bold; -fx-font-size: 16px; -fx-background-radius: 25px; -fx-background-color: #6e76aa; -fx-text-fill: WHITE;");
    }

    private void registerHandlers() {
        this.setOnAction(e -> {
            textoInfoBox.setText(registo.toString());
            infoBox.setVisible(true);
        });
    }

    private void update() {
    }
}
