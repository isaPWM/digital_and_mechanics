package pt.isec.GPS.DigitalMechanics.UI.Buttons;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import pt.isec.GPS.DigitalMechanics.AppMain;
import pt.isec.GPS.DigitalMechanics.Model.dataRecords.Peca;

import java.util.ArrayList;

public class BlocoInfoButton extends Button {
    private final Pane listaRegistosInfoPane;
    private final FlowPane listaRegistosInfoScrollPane;
    private final Label tipoRegisto, textoInfoBox;
    private final VBox infoBox;

    public BlocoInfoButton(String tipoInfo, Pane listaRegistosInfoPane, FlowPane listaRegistosInfoScrollPane, Label tipoRegisto, Label textoInfoBox, VBox infoBox){
        super(tipoInfo);
        this.listaRegistosInfoPane = listaRegistosInfoPane;
        this.listaRegistosInfoScrollPane = listaRegistosInfoScrollPane;
        this.tipoRegisto = tipoRegisto;
        this.textoInfoBox = textoInfoBox;
        this.infoBox = infoBox;
        createViews();
        registerHandlers();
        update();
    }

    private void createViews() {
        setPrefHeight(70);
        setMinWidth(100);
        setStyle("-fx-font-family: Arial Bold; -fx-font-size: 16px;-fx-background-radius: 25px; -fx-background-color: #009999; -fx-text-fill: WHITE;");
    }

    private void registerHandlers() {
        this.setOnAction(e -> {
            listaRegistosInfoScrollPane.getChildren().clear();
            preencheListaRegistos();
        });
    }

    private void update() {
    }

    private void preencheListaRegistos() {
        if (AppMain.appManager.selecionaBlocoInformacao(getText())) {
            ArrayList<Record> lista = AppMain.appManager.getListaRegistosDeBlocoAtual();
            if (!lista.isEmpty()) {
                for (Record registo : lista) {
                        if (registo instanceof Peca) {
                            listaRegistosInfoScrollPane.getChildren().add(new BotaoRegistoPeca(registo, textoInfoBox, infoBox, AppMain.appManager.getNomeBlocoInfoAtual(), listaRegistosInfoScrollPane));
                        }
                        else  {
                            listaRegistosInfoScrollPane.getChildren().add(new BotaoRegisto(registo, textoInfoBox, infoBox, AppMain.appManager.getNomeBlocoInfoAtual(), listaRegistosInfoScrollPane));
                        }
                    }
            } else
                listaRegistosInfoScrollPane.getChildren().add(new Label("Sem Registos Adicionados"));
            listaRegistosInfoPane.setVisible(true);
            tipoRegisto.setText("Registos de " + getText());
        }
    }
}
