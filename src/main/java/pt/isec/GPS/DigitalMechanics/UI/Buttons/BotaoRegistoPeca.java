package pt.isec.GPS.DigitalMechanics.UI.Buttons;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import pt.isec.GPS.DigitalMechanics.AppMain;
import pt.isec.GPS.DigitalMechanics.Model.dataRecords.Peca;

import java.io.File;

public class BotaoRegistoPeca extends HBox {
    private Peca registo;
    private Label textoInfoBox;
    private VBox infoBox;
    private Button botao;
    private FlowPane listaRegistosInfoScrollPane;
    public BotaoRegistoPeca(Record registo, Label textoInfoBox, VBox infoBox, String tipoRegisto, FlowPane listaRegistosInfoScrollPane){
        this.textoInfoBox = textoInfoBox;
        this.infoBox = infoBox;
        this.listaRegistosInfoScrollPane = listaRegistosInfoScrollPane;
        this.registo = (Peca)registo;
        createViews(tipoRegisto);
        registerHandlers();
        update();
    }

    private void createViews(String tipoRegisto) {
        botao = new Button(tipoRegisto + " em " + registo.data());
        botao.setPrefSize(250, 30);
        botao.setStyle("-fx-font-family: Arial Bold; -fx-font-size: 16px; -fx-background-radius: 25px; -fx-background-color: #6e76aa; -fx-text-fill: WHITE;");

        File image = new File(registo.foto());
        ImageView imageView;
        if (image.exists())
            imageView = new ImageView(new Image(image.toURI().toString()));
        else
            imageView = new ImageView(String.valueOf(AppMain.class.getResource("imgIcons/outra_peca.png")));
        imageView.setFitHeight(35);
        imageView.setFitWidth(35);

        setSpacing(10);
        this.getChildren().addAll(botao, imageView);
    }

    private void registerHandlers() {
        botao.setOnAction(e -> {
            textoInfoBox.setText(registo.toString());
            infoBox.setVisible(true);
        });
    }

    private void update() {
    }
}
