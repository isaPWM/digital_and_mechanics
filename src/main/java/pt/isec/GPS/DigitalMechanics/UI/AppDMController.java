package pt.isec.GPS.DigitalMechanics.UI;

import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import pt.isec.GPS.DigitalMechanics.AppMain;
import pt.isec.GPS.DigitalMechanics.Model.dataClasses.InfoRetorno;
import pt.isec.GPS.DigitalMechanics.Model.dataRecords.Peca;
import pt.isec.GPS.DigitalMechanics.UI.Buttons.BlocoInfoButton;
import pt.isec.GPS.DigitalMechanics.UI.Buttons.BotaoRegisto;
import pt.isec.GPS.DigitalMechanics.UI.Buttons.BotaoRegistoPeca;
import pt.isec.GPS.DigitalMechanics.UI.Buttons.RegistoVeiculoButton;
import pt.isec.GPS.DigitalMechanics.UI.toolsAccess.ItemComboBox;

import java.io.File;
import java.util.ArrayList;

public class AppDMController {
    private final String STYLE_DEFAULT_TF = "-fx-background-color: white; -fx-border-color: #a8accc";
    @FXML
    public ImageView imageViewPeca, imageViewOleo;
    /////////////////////////// NODES QUE SÃO BUSCADOS AO FXML VIEW ///////////////////////////
    @FXML
    private ComboBox<ItemComboBox> comboBox;
    @FXML
    private VBox infoText, erroBox, infoBox, novoBlocoNomeForm;
    @FXML
    private Button info;
    @FXML
    private Label tipoRegistos, numeroRegistos, numeroImportacoes, numeroExportacoes, textoInfoBox, registoPersonalizadoLabel;
    @FXML
    private FlowPane flowPaneRegistosVeiculo, listaBlocosInfoFlowPane, listaRegistosInfoScrollPane;
    @FXML
    private BorderPane formRegistoPersonalizado, formCriacaoVeiculo, formPeca, formAbastecimento,formmudancaOleoNovo, formManutencao;
    @FXML
    private Pane blocosInfoPane, listaRegistosInfoPane, homePane;
    @FXML
    private TextField marcaV, modeloV, matriculaV, quilometrosV, precoA, litrosA, quilometrosA,
            nomePeca, marcaPeca, modeloPeca, precoPeca, lojaPeca, marcaOleo, tipoManutencao, precoManutencao, nomeNovoBloco, nomeRegistoTF, precoPTF, descPTP;
    @FXML
    private TextArea descricaoPeca, observacaoManutencao, documentosManutencao;
    @FXML
    private DatePicker dataPeca, dataMudancaDatePicker, dataAbastecimento, dataManutencao, dataRP;
    @FXML
    private Spinner<Integer> anoV, mesV;
    @FXML
    private ComboBox<String> combustivelV;
    @FXML
    private Label informacaoErroBoxLabel;

    private boolean selectedComboPeca = false;
    private static final String caminhoImg = String.valueOf(AppMain.class.getResource("imgIcons/"));

    /////////////////////////// A EXECUTAR NA INICIALIZAÇÃO ///////////////////////////
    @FXML
    protected void initialize() {
        this.combustivelV.getItems().setAll("Gasolina Comum", "Gasolina Aditiva", "Gasolina Premium",
                                          "Etanol", "Etanol Aditivo", "GNV", "GPL", "Diesel Comum",
                                          "Diesel Aditivo", "Diesel Premium");
        this.combustivelV.setValue(" ");
        this.flowPaneRegistosVeiculo.setPadding(new Insets(10));
        this.flowPaneRegistosVeiculo.setHgap(10);
        this.flowPaneRegistosVeiculo.setVgap(10);
        atualizaFlowPaneRegistosVeiculos();

        /////////////////////// COMBOBOX PECA ///////////////////////////////////////

        comboBox.getItems().addAll(
                new ItemComboBox("Turbo",caminhoImg+"turbo.png"),
                new ItemComboBox("Pneu",caminhoImg+"pneu.png"),
                new ItemComboBox("Vela",caminhoImg+"vela.png"),
                new ItemComboBox("Bateria",caminhoImg+"bateria.png"),
                new ItemComboBox("Amortecedor",caminhoImg+"suspensao.png"),
                new ItemComboBox("Lampada",caminhoImg+"luz_para_farois.png"),
                new ItemComboBox("Relé",caminhoImg+"rele.png"),
                new ItemComboBox("Pastilha",caminhoImg+"pinca_travoes.png"),
                new ItemComboBox("Radiador",caminhoImg+"radiador.png"),
                new ItemComboBox("Para-Brisas",caminhoImg+"para_brisas.png"),
                new ItemComboBox("Faróis",caminhoImg+"farois.png"),
                new ItemComboBox("Fusível",caminhoImg+"fusivel.png"),
                new ItemComboBox("Travões",caminhoImg+"travoes.png"),
                new ItemComboBox("Embraiagem",caminhoImg+"disco_embreagem.png"),
                new ItemComboBox("Correia de distribuição",caminhoImg+"correia_distribuicao.png"),
                new ItemComboBox("Correia de alternador",caminhoImg+"correia_alternador.png"),
                new ItemComboBox("Filtro do ar",caminhoImg+"filtro_ar_comum.png"),
                new ItemComboBox("Filtro do óleo",caminhoImg+"filtro_oil.png"),
                new ItemComboBox("Filtro do combustível",caminhoImg+"filtro_gas.png"),
                new ItemComboBox("Outro",caminhoImg+"outra_peca.png")
        );
        comboBox.setCellFactory(param -> new ItemComboBox.ListCell());
        comboBox.setButtonCell(new ItemComboBox.ListCell());
        comboBox.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null)
                selectedComboPeca = !newValue.getImage().getUrl().contains(caminhoImg + "outra_peca.png");
        });
    }

/////////////////////////// INFO ADICIONAL, ERRO BOX, VOLTAR ///////////////////////////
    @FXML
    public void info() {
        infoText.setVisible(!infoText.isVisible());
        numeroExportacoes.setText("" + AppMain.appManager.getNumExportacoes());
        numeroImportacoes.setText("" + AppMain.appManager.getNumImportacoes());
        numeroRegistos.setText("" + AppMain.appManager.getNumVeiculos());
    }
    @FXML
    public void fecharErroBox() {
        erroBox.setVisible(false);
    }

    @FXML
    public void fecharInfoBox() {
        infoBox.setVisible(false);
    }

    public void voltar() {
        if (blocosInfoPane.isVisible()) {
            blocosInfoPane.setVisible(false);
            AppMain.appManager.desSelecionaRegistoVeiculo();
        }
    }

    public void voltarRegistPane() {
        if (listaRegistosInfoPane.isVisible()) {
            listaRegistosInfoPane.setVisible(false);
        }
    }

/////////////////////////// REGISTO VEÍCULO ////////////////////////////////////////////////////

    /////////////////////////// FUNÇÃO PARA PÔR O FORM DE CRIA REGISTO VISÍVEL ////////////////
    @FXML
    protected void visibilidadeRegistoVeiculoForm() {
        formCriacaoVeiculo.setVisible(!formCriacaoVeiculo.isVisible());
        homePane.setDisable(!homePane.isDisabled());
    }

    /////////////////////////// FUNÇÕES REGISTO DE VEÍCULO ///////////////////////////
    @FXML
    protected void adicionaRegistoVeiculo() {
        if(AppMain.appManager.adicionaRegistoVeiculo(marcaV.getText(), modeloV.getText(),
                matriculaV.getText(), combustivelV.getValue(),
                anoV.getValue(), mesV.getValue(), quilometrosV.getText()))
            System.out.println("sim"); //msgBox("true");

        visibilidadeRegistoVeiculoForm();
        atualizaFlowPaneRegistosVeiculos();
        /*else {
            if (res[0]) {
                visibilidadeRegistoVeiculoForm();
                atualizaFlowPaneRegistosVeiculos();
                marcaV.clear(); modeloV.clear(); matriculaV.clear(); quilometrosV.clear();
            } else {
                msgBox(false);
            }
            marcaV.setStyle("-fx-background-color: " + (res[1] ? "white" : "#FFCDD2"));
            modeloV.setStyle("-fx-background-color: " + (res[2] ? "white" : "#FFCDD2"));
            matriculaV.setStyle("-fx-background-color: " + (res[3] ? "white" : "#FFCDD2"));
            quilometrosV.setStyle("-fx-background-color: " + (res[3] ? "white" : "#FFCDD2"));
        }*/
        System.out.println("nao");
    }
    private void atualizaFlowPaneRegistosVeiculos(){
        flowPaneRegistosVeiculo.getChildren().clear();
        String[] lista = AppMain.appManager.getNomesRegistosVeiculos();
        if(lista.length == 0)
            flowPaneRegistosVeiculo.getChildren().add(new Label("Sem Registos de Veículos"));
        else {
            for (String registo : lista) {
                flowPaneRegistosVeiculo.getChildren().add(new RegistoVeiculoButton(registo, blocosInfoPane, listaBlocosInfoFlowPane, listaRegistosInfoPane, listaRegistosInfoScrollPane, tipoRegistos, textoInfoBox, infoBox));
            }
        }
    }


/////////////////////////// BLOCOS INFORMAÇÃO ////////////////////////////////////////////////////
    @FXML
    public void visibilidadeDeFormsDeRegisto() {
        switch (AppMain.appManager.getTipoBlocoInfo()) {
            case PECAS -> {
                nomePeca.setStyle(STYLE_DEFAULT_TF);
                marcaPeca.setStyle(STYLE_DEFAULT_TF);
                modeloPeca.setStyle(STYLE_DEFAULT_TF);
                precoPeca.setStyle(STYLE_DEFAULT_TF);
                formPeca.setVisible(!formPeca.isVisible());
            }
            case ABASTECIMENTOS -> {
                precoA.setStyle(STYLE_DEFAULT_TF);
                litrosA.setStyle(STYLE_DEFAULT_TF);
                quilometrosA.setStyle(STYLE_DEFAULT_TF);
                dataAbastecimento.setStyle(STYLE_DEFAULT_TF);
                formAbastecimento.setVisible(!formAbastecimento.isVisible());
            }
            case MUDANCAOLEO -> {
                dataMudancaDatePicker.setStyle(STYLE_DEFAULT_TF);
                marcaOleo.setStyle(STYLE_DEFAULT_TF);
                formmudancaOleoNovo.setVisible(!formmudancaOleoNovo.isVisible());
            }
            case MANUTENCOES -> {
                tipoManutencao.setStyle(STYLE_DEFAULT_TF);
                dataManutencao.setStyle(STYLE_DEFAULT_TF);
                precoManutencao.setStyle(STYLE_DEFAULT_TF);
                formManutencao.setVisible(!formManutencao.isVisible());
            }
            case OUTRO -> {
                nomeRegistoTF.setStyle(STYLE_DEFAULT_TF);
                dataRP.setStyle(STYLE_DEFAULT_TF);
                precoPTF.setStyle(STYLE_DEFAULT_TF);
                registoPersonalizadoLabel.setText("Registos de " + AppMain.appManager.getNomeBlocoInfoAtual());
                formRegistoPersonalizado.setVisible(!formRegistoPersonalizado.isVisible());
            }
        }
        listaRegistosInfoPane.setDisable(!listaRegistosInfoPane.isDisabled());
    }
    /////////////////////////// FUNÇÃO ADICIONAR BLOCO INFO PERSONALIZADO ////////////////////////

    @FXML
    public void visibilidadeNomeDeNovoBlocoRP() {
        novoBlocoNomeForm.setVisible(!novoBlocoNomeForm.isVisible());
        blocosInfoPane.setDisable(!blocosInfoPane.isDisabled());
    }
    private void atualizaFlowPaneBlocosInfo(String nomeBlocoInfoNovo){
        listaBlocosInfoFlowPane.getChildren().add(new BlocoInfoButton(nomeBlocoInfoNovo, listaRegistosInfoPane, listaRegistosInfoScrollPane, tipoRegistos, textoInfoBox, infoBox));
        blocosInfoPane.setVisible(true);
    }
    @FXML
    public void adicionaBlocoInformacao() {
        if(AppMain.appManager.adicionaBlocoInformacao(nomeNovoBloco.getText())) {
            visibilidadeNomeDeNovoBlocoRP();
            atualizaFlowPaneBlocosInfo(nomeNovoBloco.getText());
        }
        else {
            erroBox.setVisible(true);
        }
    }

/////////////////////////// REGISTOS DE INFORMAÇÃO ////////////////////////////////////////////////////
    private void msgBox(String msg){
        erroBox.setVisible(true);
        informacaoErroBoxLabel.setText(msg);
    }

    /////////////////////////// FUNÇÃO ADICIONAR NOVO REGISTO DE ABASTECIMENTO ////////////////////////
    @FXML
    public void adicionaAbastecimento() {
        InfoRetorno res = AppMain.appManager.adicionaAbastecimento(precoA.getText(), litrosA.getText(), quilometrosA.getText(), dataAbastecimento.getValue());
        if(res == null) {
            msgBox("Tente novamente...");
        }
        else {
            if (res.resultado()) {
                visibilidadeDeFormsDeRegisto();
                atualizaListaRegistosInfoScrollPane();
                precoA.clear(); litrosA.clear(); quilometrosA.clear(); dataAbastecimento.setValue(null);
            } else {
                msgBox(res.mensagem());
            }
            precoA.setStyle(res.validadeParametros()[0] ? STYLE_DEFAULT_TF : "-fx-background-color: #FFCDD2");
            litrosA.setStyle(res.validadeParametros()[1] ? STYLE_DEFAULT_TF : "-fx-background-color: #FFCDD2");
            quilometrosA.setStyle(res.validadeParametros()[2] ? STYLE_DEFAULT_TF : "-fx-background-color: #FFCDD2");
            dataAbastecimento.setStyle(res.validadeParametros()[3] ? STYLE_DEFAULT_TF : "-fx-background-color: #FFCDD2");
        }
    }

    /////////////////////////// FUNÇÃO ADICIONAR NOVO REGISTO DE PECA ////////////////////////
    @FXML
    public void adicionaPeca() {
        String auxImage;
        if(imageViewPeca.getImage() != null)
            auxImage = imageViewPeca.getImage().getUrl();
        else
            if(selectedComboPeca)
                auxImage = comboBox.getValue().getImage().getUrl();
            else
                auxImage = caminhoImg + "outra_peca.png";

        InfoRetorno res = AppMain.appManager.adicionaPeca(nomePeca.getText(), marcaPeca.getText(), modeloPeca.getText(), precoPeca.getText(),
                descricaoPeca.getText(), lojaPeca.getText(), dataPeca.getValue(), auxImage);
        if(res == null) {
            msgBox("Tente novamente...");
        }
        else {
            if (res.resultado()) {
                visibilidadeDeFormsDeRegisto();
                atualizaListaRegistosInfoScrollPane();
                nomePeca.clear(); marcaPeca.clear(); modeloPeca.clear(); precoPeca.clear(); descricaoPeca.clear(); lojaPeca.clear(); dataPeca.setValue(null);
            } else {
                msgBox(res.mensagem());
            }
            nomePeca.setStyle(res.validadeParametros()[0] ? STYLE_DEFAULT_TF : "-fx-background-color: #FFCDD2");
            marcaPeca.setStyle(res.validadeParametros()[0] ? STYLE_DEFAULT_TF : "-fx-background-color: #FFCDD2");
            modeloPeca.setStyle(res.validadeParametros()[0] ? STYLE_DEFAULT_TF : "-fx-background-color: #FFCDD2");
            precoPeca.setStyle(res.validadeParametros()[0] ? STYLE_DEFAULT_TF : "-fx-background-color: #FFCDD2");
        }
    }

    ///////////////////////// FUNÇÃO MANUT ///////////////////////////////////
    @FXML
    public void adicionaManutencao(){
        InfoRetorno res = AppMain.appManager.adicionaManutencao(tipoManutencao.getText(), dataManutencao.getValue(),
                precoManutencao.getText(), observacaoManutencao.getText());
        if(res == null) {
            msgBox("Tente novamente...");
        }
        else {
            if (res.resultado()) {
                visibilidadeDeFormsDeRegisto();
                atualizaListaRegistosInfoScrollPane();
                tipoManutencao.clear(); dataManutencao.setValue(null); precoManutencao.clear(); observacaoManutencao.clear();
            } else {
                msgBox(res.mensagem());
            }
            tipoManutencao.setStyle(res.validadeParametros()[0] ? STYLE_DEFAULT_TF : "-fx-background-color: #FFCDD2");
            dataManutencao.setStyle(res.validadeParametros()[0] ? STYLE_DEFAULT_TF : "-fx-background-color: #FFCDD2");
            precoManutencao.setStyle(res.validadeParametros()[0] ? STYLE_DEFAULT_TF : "-fx-background-color: #FFCDD2");
        }
    }

    /////////////////////////// FUNÇÃO ADICIONAR NOVO REGISTO DE MUDANCA DE OLEO ////////////////////////
    @FXML
    public void mudancaOleoCriarRegistro() {
        String auxImage;
        if(imageViewOleo.getImage() != null)
            auxImage = imageViewOleo.getImage().getUrl();
        else
            auxImage = caminhoImg + "outra_peca.png";

        InfoRetorno res = AppMain.appManager.adicionaMudancaOleo(dataMudancaDatePicker.getValue(),marcaOleo.getText(),auxImage);
        if(res == null) {
            msgBox("Tente novamente...");
        }
        else {
            if (res.resultado()) {
                visibilidadeDeFormsDeRegisto();
                atualizaListaRegistosInfoScrollPane();
                marcaOleo.clear(); dataMudancaDatePicker.setValue(null);
            } else {
                msgBox(res.mensagem());
            }
            dataMudancaDatePicker.setStyle(res.validadeParametros()[0] ? STYLE_DEFAULT_TF : "-fx-background-color: #FFCDD2");
            marcaOleo.setStyle(res.validadeParametros()[0] ? STYLE_DEFAULT_TF : "-fx-background-color: #FFCDD2");
        }
    }

    /////////////////////////// FUNÇÃO ADICIONAR NOVO REGISTO P ////////////////////////
    @FXML
    public void adicionaRegistoPersonalizado(){
        InfoRetorno res = AppMain.appManager.adicionaRegistoPersonalizado(nomeRegistoTF.getText(), dataRP.getValue(), precoPTF.getText(), descPTP.getText());
        if(res == null) {
            msgBox("Tente novamente...");
        }
        else {
            if (res.resultado()) {
                visibilidadeDeFormsDeRegisto();
                atualizaListaRegistosInfoScrollPane();
                nomeRegistoTF.clear(); dataRP.setValue(null); precoPTF.clear(); descPTP.clear();
            } else {
                msgBox(res.mensagem());
            }
            nomeRegistoTF.setStyle(res.validadeParametros()[0] ? STYLE_DEFAULT_TF : "-fx-background-color: #FFCDD2");
            dataRP.setStyle(res.validadeParametros()[0] ? STYLE_DEFAULT_TF : "-fx-background-color: #FFCDD2");
            precoPTF.setStyle(res.validadeParametros()[0] ? STYLE_DEFAULT_TF : "-fx-background-color: #FFCDD2");
        }
    }

    /////////////////////////// FUNÇÃO ATUALIZAR LISTA DE REGISTOS DE INFORMAÇÃO //////////////
    @FXML
    private void atualizaListaRegistosInfoScrollPane() {
        listaRegistosInfoScrollPane.getChildren().clear();
        ArrayList<Record> lista = AppMain.appManager.getListaRegistosDeBlocoAtual();
        if (!lista.isEmpty()) {
            for (Record registo : lista) {
                if (registo instanceof Peca) {
                    listaRegistosInfoScrollPane.getChildren().add(new BotaoRegistoPeca(registo, textoInfoBox, infoBox, AppMain.appManager.getNomeBlocoInfoAtual(), listaRegistosInfoScrollPane));
                }
                else  {
                    listaRegistosInfoScrollPane.getChildren().add(new BotaoRegisto(registo, textoInfoBox, infoBox, AppMain.appManager.getNomeBlocoInfoAtual(), listaRegistosInfoScrollPane));
                }
            }
        } else
            listaRegistosInfoScrollPane.getChildren().add(new Label("Sem Registos Adicionados"));
        listaRegistosInfoPane.setVisible(true);
    }


    //////////////////////////// FUNÇÃO ABRIR FILE CHOOSER /////////////////////////////////////////
    public void abrirFileChooser() {
        FileChooser fileChooser = new FileChooser();

        // Configuração para exibir apenas arquivos de imagem
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Imagens", "*.png", "*.jpg", "*.jpeg", "*.gif");
        fileChooser.getExtensionFilters().add(extFilter);

        // Mostra a janela de seleção do arquivo e obtém o arquivo selecionado
        Stage stage = (Stage) (blocosInfoPane).getScene().getWindow();
        File arquivoSelecionado = fileChooser.showOpenDialog(stage);

        if (arquivoSelecionado != null) {
            // Carrega a imagem selecionada e exibe na ImageView
            Image imagem = new Image(arquivoSelecionado.toURI().toString());
            imageViewPeca.setImage(imagem);
            imageViewOleo.setImage(imagem);
        }
    }
}
