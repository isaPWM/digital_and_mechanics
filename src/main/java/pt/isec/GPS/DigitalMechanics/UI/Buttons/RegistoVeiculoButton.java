package pt.isec.GPS.DigitalMechanics.UI.Buttons;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import pt.isec.GPS.DigitalMechanics.AppMain;

public class RegistoVeiculoButton extends Button {
    private final FlowPane listaBlocosInfoFlowPane, listaRegistosInfoScrollPane;
    private final Pane blocosInfoPane, listaRegistosInfoPane;
    private final Label tipoRegisto, textoInfoBox;
    private final VBox infoBox;

    public RegistoVeiculoButton(String registo, Pane blocosInfoPane, FlowPane listaBlocosInfoFlowPane, Pane listaRegistosInfoPane, FlowPane listaRegistosInfoScrollPane, Label tipoRegisto, Label textoInfoBox, VBox infoBox){
        super(registo);
        this.listaRegistosInfoPane = listaRegistosInfoPane;
        this.blocosInfoPane = blocosInfoPane;
        this.listaBlocosInfoFlowPane = listaBlocosInfoFlowPane;
        this.listaRegistosInfoScrollPane = listaRegistosInfoScrollPane;
        this.tipoRegisto = tipoRegisto;
        this.textoInfoBox = textoInfoBox;
        this.infoBox = infoBox;
        createViews();
        registerHandlers();
        update();
    }
    private void createViews() {
        setPrefHeight(90);
        setStyle("-fx-font-family: Arial Bold; -fx-font-size: 18px;-fx-background-radius: 20%; -fx-background-color: #006064; -fx-text-fill: WHITE;");
    }

    private void registerHandlers() {
        this.setOnAction(e -> {
            AppMain.appManager.selecionaRegistoVeiculo(getText());
            listaBlocosInfoFlowPane.getChildren().clear();
            String[] lista = AppMain.appManager.getNomesBlocosInfo();
            if (lista.length == 0) {
                listaBlocosInfoFlowPane.getChildren().add(new Label("Sem blocos de informação registados"));
            } else {
                for (String registo : lista) {
                    listaBlocosInfoFlowPane.getChildren().add(new BlocoInfoButton(registo, listaRegistosInfoPane, listaRegistosInfoScrollPane, tipoRegisto, textoInfoBox, infoBox));
                }
            }
            blocosInfoPane.setVisible(true);
        });
    }
    private void update() {
    }
}