module com.example.tp_gps_digitalmechanics {
    requires javafx.graphics;
    requires javafx.fxml;

    requires org.controlsfx.controls;
    requires com.dlsc.formsfx;
    requires net.synedra.validatorfx;
    requires org.kordamp.ikonli.javafx;
    requires org.kordamp.bootstrapfx.core;
    //requires eu.hansolo.tilesfx;
    //requires com.almasb.fxgl.all;

    opens pt.isec.GPS.DigitalMechanics to javafx.fxml;
    exports pt.isec.GPS.DigitalMechanics;
    opens pt.isec.GPS.DigitalMechanics.UI to javafx.fxml;
    exports pt.isec.GPS.DigitalMechanics.UI;
    opens pt.isec.GPS.DigitalMechanics.Model to javafx.fxml;
    exports pt.isec.GPS.DigitalMechanics.Model;
    exports pt.isec.GPS.DigitalMechanics.Model.dataClasses;
    opens pt.isec.GPS.DigitalMechanics.Model.dataClasses to javafx.fxml;
    exports pt.isec.GPS.DigitalMechanics.UI.Buttons;
    opens pt.isec.GPS.DigitalMechanics.UI.Buttons to javafx.fxml;
}